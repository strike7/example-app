﻿namespace STRIKE7_API_Examples
{
    using System.Drawing;
    using Strike7API;

    public class MapApplet : Strike7Applet
    {
        private Bitmap m_map = null;
        private int m_x = 0;
        private int m_y = 0;
        private int m_viewWidth = 480;
        private int m_viewHeight = 320;
        private int m_mapWidth;
        private int m_mapHeight;

        private bool m_pressed = false;
        private int m_lastTouchX;
        private int m_lastTouchY;

        private int m_scrollX = 0;
        private int m_scrollY = 0;

        private object m_screenUpdateLock = new object();

        private System.Timers.Timer m_screenUpdateTimer = new System.Timers.Timer(25);

        private System.OperatingSystem m_osInfo = System.Environment.OSVersion;


        private void ClearTextData()
        {
            lock (m_screenUpdateLock)
            {
                UpdateMapSection(0, 0, m_x, m_y, 180, 60);
                UpdateMapSection(0, 40, m_x, m_y + 40, 420, 20);
            }
        }
        private void AddTextData()
        {
            lock (m_screenUpdateLock)
            {
                DrawText(new Point(0, 0), 18, Color.Black, "Date: " + System.DateTime.Now.ToLongDateString());
                DrawText(new Point(0, 20), 18, Color.Black, "Time: " + System.DateTime.Now.ToLongTimeString());
                DrawText(new Point(0, 40), 18, Color.Black, "OS: " + m_osInfo.ToString());
            }
        }

        public MapApplet(Bitmap map)
        {
            m_map = map;
            m_mapWidth = m_map.Width;
            m_mapHeight = m_map.Height;

            m_screenUpdateTimer.Elapsed += Event_ScreenUpdateTimer_Elapsed;
            m_screenUpdateTimer.AutoReset = true;
        }

        public override bool Open(string name, Bitmap image)
        {
            return base.Open(name, image);
        }

        public override void Close()
        {
            m_screenUpdateTimer.Stop();
            base.Close();
        }

        public override void OnAppletActive(int endPointId, long endPointType)
        {
            base.OnAppletActive(endPointId, endPointType);
            UpdateWholeMap();
        }

        public override void OnAppletInactive(int endPointId)
        {
            base.OnAppletInactive(endPointId);
            m_screenUpdateTimer.Stop();
        }

        public override void OnAppletReset(int endPointId)
        {
            base.OnAppletReset(endPointId);
            m_screenUpdateTimer.Stop();
        }

        public override void OnAppletTouch(int endPointId, int x, int y, bool pressed)
        {
            base.OnAppletTouch(endPointId, x, y, pressed);

            if (m_pressed && pressed)
            {
                lock (m_screenUpdateLock)
                {
                    m_scrollX += m_lastTouchX - x;
                    m_scrollY += m_lastTouchY - y;
                }
            }

            if (m_pressed && !pressed)
            {
                m_screenUpdateTimer.Stop();
                UpdateScreenForScroll();
            }

            if (!m_pressed && pressed)
            {
                m_screenUpdateTimer.Start();
            }

            m_pressed = pressed;
            m_lastTouchX = x;
            m_lastTouchY = y;
        }

        public void UpdateScreenForScroll()
        {
            int scrollX;
            int scrollY;

            lock (m_screenUpdateLock)
            {
                scrollX = m_scrollX;
                scrollY = m_scrollY;

                m_scrollX = 0;
                m_scrollY = 0;
            }

            if (scrollX + scrollY == 0) return;

            lock (m_screenUpdateTimer)
            {
                ClearTextData();
                ScrollMap(scrollX, scrollY);
                AddTextData();
                // Sends the screen update command.
                UpdateScreen();
            }
        }

        private void UpdateWholeMap()
        {
            UpdateMapSection(0, 0, m_x, m_y, m_viewWidth, m_viewHeight);
            UpdateScreen();
        }

        private void UpdateMapSection(int x, int y, int fromX, int fromY, int width, int height)
        {
            Draw(x, y, fromX, fromY, width, height, m_map);
        }

        private void ScrollMap(int x, int y)
        {
            m_x += x;
            m_y += y;

            // Check bounds.
            if (m_x < 0)
            {
                m_x -= x;
                x = m_x * -1;
                m_x = 0;
            }

            if (m_y < 0)
            {
                m_y -= y;
                y = m_y * -1;
                m_y = 0;
            }

            if ((m_x + m_viewWidth) > m_mapWidth)
            {
                m_x -= x;
                x = m_mapWidth - (m_x + m_viewWidth);
                m_x += x;
            }

            if ((m_y + m_viewHeight) > m_mapHeight)
            {
                m_y -= y;
                y = m_mapHeight - (m_y + m_viewHeight);
                m_y += y;
            }

            // Scroll
            Scroll(0, 0, m_viewWidth, m_viewHeight, x * -1, y * -1);

            // Repaint

            // X (width) portion of the update
            int x_startX = 0;
            int x_startY = 0;
            int x_width = 0;
            int x_height = 0;
            int x_fromX = 0;
            int x_fromY = 0;

            // Y (height) portion of the update
            int y_startX = 0;
            int y_startY = 0;
            int y_width = 0;
            int y_height = 0;
            int y_fromX = 0;
            int y_fromY = 0;

            // The little block that intersects the X&Y portions of the update
            int b_startX = 0;
            int b_startY = 0;
            int b_width = 0;
            int b_height = 0;
            int b_fromX = 0;
            int b_fromY = 0;

            // Calcualtions per quadrant
            if (x >= 0 && y >= 0)
            {
                b_width = x;
                b_height = y;
                b_startX = m_viewWidth - x;
                b_startY = m_viewHeight - y;
                b_fromX = m_x + b_startX;
                b_fromY = m_y + b_startY;
            }
            else if (x >= 0 && y < 0)
            {
                b_width = x;
                b_height = y * -1;
                b_startX = m_viewWidth - x;
                b_startY = 0;
                b_fromX = m_x + b_startX;
                b_fromY = m_y;
            }
            else if (x < 0 && y >= 0)
            {
                b_width = x * -1;
                b_height = y;
                b_startX = 0;
                b_startY = m_viewHeight - b_height;
                b_fromX = m_x;
                b_fromY = m_y + b_startY;
            }
            else if (x < 0 && y < 0)
            {
                b_width = x * -1;
                b_height = y * -1;
                b_startX = 0;
                b_startY = 0;
                b_fromX = m_x;
                b_fromY = m_y;
            }

            if (x >= 0)
            {
                x_width = b_startX;
                x_height = b_height;
                x_startX = 0;
                x_startY = b_startY;
                x_fromX = m_x;
                x_fromY = b_fromY;
            }
            else
            {
                x_width = m_viewWidth - b_width;
                x_height = b_height;
                x_startX = b_width;
                x_startY = b_startY;
                x_fromX = m_x + b_width;
                x_fromY = m_y + b_startY;
            }

            if (y >= 0)
            {
                y_width = b_width;
                y_height = b_startY;
                y_startX = b_startX;
                y_startY = 0;
                y_fromX = b_fromX;
                y_fromY = m_y;
            }
            else
            {
                y_width = b_width;
                y_height = m_viewHeight - b_height;
                y_startX = b_startX;
                y_startY = b_height;
                y_fromX = b_fromX;
                y_fromY = m_y + b_height;
            }

            if (x_width > 0 && x_height > 0)
            {
                UpdateMapSection(x_startX, x_startY, x_fromX, x_fromY, x_width, x_height);
            }

            if (y_width > 0 && y_height > 0)
            {
                UpdateMapSection(y_startX, y_startY, y_fromX, y_fromY, y_width, y_height);
            }

            if (b_width > 0 && b_height > 0)
            {
                UpdateMapSection(b_startX, b_startY, b_fromX, b_fromY, b_width, b_height);
            }
        }

        private void Event_ScreenUpdateTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            UpdateScreenForScroll();
        }
    }
}
