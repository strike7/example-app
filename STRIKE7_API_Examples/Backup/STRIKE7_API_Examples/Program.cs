﻿namespace STRIKE7_API_Examples
{
    using System;
    using STRIKE7_API_Examples.Properties;

    public class Program
    {
        public static void Main(string[] args)
        {
            MapApplet mapApplet = new MapApplet(Resources.MadCatz);            
            mapApplet.Open("Map", Resources.MadCatzIcon);

            Console.WriteLine("Running: AppletId: {0}", mapApplet.Id);
            Console.WriteLine("Press any key to exit.");

            Console.ReadKey();

            mapApplet.Close();
        }
    }
}
