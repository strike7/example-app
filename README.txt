S.T.R.I.K.E.7 API Primer
========================

System Requirements
-------------------
* PC
* IDE (Visual Studio 2008 Express or better)
* A PDF viewer.
* Programming knowledge in C++ or C#
* A S.T.R.I.K.E.7 Keyboard.

Introduction
------------
These APIs are not intended for anyone without prior programming experience. However, it is not a difficult API to understand or interact with. Most of the programming work will be in the applet code itself.

The APIs allow a programmer to register an applet (plus an image) with the S.T.R.I.K.E.7 system. The image will appear on the S.T.R.I.K.E.7 Simulator program (provided with the API). When a user taps on the icon, the applet will be allowed to render images to the S.T.R.I.K.E.7 V.E.N.O.M. screen (simulator). 

The API provides a set of graphics operations for the applet to send to the V.E.N.O.M. screen and sends touch screen information back to the applet code. All the program logic is resident in the applet and the applet can update the graphics on the V.E.N.O.M. screen via the API.

The S.T.R.I.K.E.7 system uses a program called the multiplexer, to connect the API to the rest of the system. The multiplexer must be running to allow the API to connect to the S.T.R.I.K.E.7.

There are two API dlls provided with this release. The first Strike7API.dll is a C++ dll. The second Strike7API.net.dll is a .net wrapper for the Strike7API.dll.

When using the Strike7API.net.dll you must also copy the Strike7API.dll in to the same directory. Also when building a .net applet the Target platform must be set to x86. Otherwise your program will throw a 'BadImageFormatException'.

System setup
------------
Please remove any S.T.R.I.K.E.7 Sentinel software and programming software that is currently installed on your system.

* Run the S.T.R.I.K.E.7 firmeware update program.WARNING! Firmware Update program is currently not compatible with Windows 8.
* Run the driver install.
* Run the software install to enable macro programming.

Directory structure
-------------------

Doc
---
You will find both the STRIKE7 and STRIKE7.net API specification documents here. The documents are provided in PDF format.

STRIKE7_API_Examples
--------------------
Contains example code for both versions of the API. A precompiled version of the code with both the API dlls is provided in the "STRIKE7 API\STRIKE7_API_Examples\STRIKE7_API_Examples\bin\Debug" directory.

MapApplet.cs - This applet was originally use to scroll a map around the VEMON screen (Hence the name). However this applet just allows the user to drag an image that is bigger than the VEMON screen around. If you navigate to the �STRIKE7 API\STRIKE7_API_Examples\STRIKE7_API_Examples\Resources� directory you will see the image that is used for this application (Map.png). Just over write this file and recompile the application to replace the image with one of your own.

APIDlls
-------
Contains both the API dlls.

